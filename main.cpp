#include <iostream>
#include <vector>

using namespace std;

class Stack_with_min {
    public:
        void push(int value) {
            values_.push_back(value);
            if (min_value_.empty())
                min_value_.push_back(value);
            else {
                int minimum = min_value_.back();
                min_value_.push_back(min(value, minimum));
            }
        }

        void pop() {
            values_.pop_back();
            min_value_.pop_back();
        }

        int back() {
            return values_.back();
        }

        int get_min() {
            return min_value_.back();
        }

        size_t size() {
            return values_.size();
        }

        bool empty() {
            return values_.empty();
        }

        void clear() {
            values_.clear();
            min_value_.clear();
        }
    private:
        vector <int> values_;
        vector <int> min_value_;
};

class Queue_with_min {
    public:
        void push(int value) {
            head.push(value);
        }

        void pop() {
            if (tail.empty())
                while (!head.empty()) {
                    tail.push(head.back());
                    head.pop();
                }
            tail.pop();//queue size always should be > 0
        }

        int get_min() {
            if (head.empty())
                return tail.get_min();
            else if (tail.empty())
                return head.get_min();
            else {
                int head_min = head.get_min();
                int tail_min = tail.get_min();
                return min(head_min, tail_min);
            }
        }

        size_t size() {
            return head.size() + tail.size();
        }

        void clear() {
            tail.clear();
            head.clear();
        }
    private:
        Stack_with_min head, tail;
};

int main() {
    return 0;
}
